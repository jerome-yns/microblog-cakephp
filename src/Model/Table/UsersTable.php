<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FollowsTable&\Cake\ORM\Association\HasMany $Follows
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\RetweetTable&\Cake\ORM\Association\HasMany $Retweet
 * @property \App\Model\Table\TweetsTable&\Cake\ORM\Association\HasMany $Tweets
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);


        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Follows', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Retweet', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Tweets', [
            'foreignKey' => 'user_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->add('username', [
                'length' => [
                    'rule' => ['minLength', 2],
                    'message' => 'Username must be at least 2 characters long',
                ]
            ])
            ->maxLength('username', 20)
            ->requirePresence('username', 'create')
            ->notEmptyString('username')
            ->add('username',[
                'characters' => [
                    'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
                    'message'  => 'Alphanumeric characters only'
                ],
            ] );

        $validator
            ->alphaNumeric('first_name')
            ->add('first_name', [
                'length' => [
                    'rule' => ['minLength', 2],
                    'message' => 'First name must be at least 2 characters long',
                ]
            ])
            ->maxLength('first_name', 20)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name')
            ->add('first_name',[
                'characters' => [
                    'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
                    'message'  => 'Alphanumeric characters only'
                ],
            ] );

        $validator
            ->alphaNumeric('last_name')
            ->add('last_name', [
                'length' => [
                    'rule' => ['minLength', 2],
                    'message' => 'Last name must be at least 2 characters long',
                ]
            ])
            ->maxLength('last_name', 20)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name')
            ->add('last_name',[
                'characters' => [
                    'rule' => array('custom', '/^[a-z0-9 ]*$/i'),
                    'message'  => 'Alphanumeric characters only'
                ],
            ] );

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email');

        $validator
            ->scalar('password')
            ->add('password', [
                'length' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Password must be at least 6 characters long',
                ]
            ])
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmptyString('password')
            ->add('password',[
                'characters' => [
                    'rule' => array('custom', '/^[a-z0-9-$-_.]*$/i'),
                    'message'  => 'Alphanumeric characters only'
                ],
            ] );

        $validator
            ->allowEmptyFile('image_file')
            ->add('image_file',[
            'validExtension' => [
                'rule' => ['extension',['png','jpg','jpeg']], // default  ['gif', 'jpeg', 'png', 'jpg']
                'message' => __('These files extension are not allowed')
            ],
            'fileSize' => [
                'rule' => [ 'fileSize', '<=', '1MB' ],
                'message' => 'Image file size must be less than 1MB.',
            ],
        ] );
        

        $validator
            ->dateTime('verified')
            ->allowEmptyDateTime('verified');

        $validator
            ->dateTime('deleted')
            ->allowEmptyDateTime('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['username']), ['errorField' => 'username']);
        $rules->add($rules->isUnique(['email']), ['errorField' => 'email']);

        return $rules;
    }
}

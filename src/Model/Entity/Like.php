<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Like Entity
 *
 * @property int $id
 * @property int $tweet_id
 * @property int $user_id
 * @property int|null $retweet_id
 *
 * @property \App\Model\Entity\Tweet $tweet
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Retweet $retweet
 */
class Like extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tweet_id' => true,
        'user_id' => true,
        'retweet_id' => true,
        'tweet' => true,
        'user' => true,
        'retweet' => true,
    ];
}

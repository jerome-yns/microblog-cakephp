<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;
use Cake\I18n\Time;
use Cake\Mailer\Mailer;
use Cake\View\Helper\HtmlHelper; 

class UsersController extends AppController
{
    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('Homepage');
    }
         
    public function login()
    {
        $this->viewBuilder()->setLayout('users');
        $this->request->allowMethod(['get', 'post']);
        if ($this->request->is('post')) {
            $is_verified = $this->Users->find()->select('verified')->where(['email' => $this->request->getData('email')])->first();
            if ($is_verified && $is_verified['verified']){
                $result = $this->Authentication->getResult();
                if ($result->isValid()) {
                    
                    return $this->redirect(['controller' => 'Users', 'action' => 'index']);
                }
            } 
            $this->Flash->error(__('Invalid username or password.'));

            return $this->redirect(['controller' => 'Users', 'action' => 'logout']);
        }
    }

    public function search()
    {
        $html = new HtmlHelper(new \Cake\View\View());
        if ($this->request->getData('query')){
            $inputText = $this->request->getData('query');
            $result = $this->Users->find()
                ->where(['username LIKE' =>'%'.$inputText.'%'])->all();

            if ($result->count()>0){
                foreach ($result as $res) {
                    echo "<div class='d-flex align-items-center list-group-item list-group-item-action-border-1 px-3' id='lists-user'>";
                        if ($res->profile_pic){
                             echo  $html->image($res->profile_pic,['class' => 'nav_userimage']);
                        }
                        echo $html->link(h(ucwords($res->username)), ['controller'=> 'users', 'action' => 'profile',$res->id]);
                    echo "</div>";
                }
                    echo "<div class='viewlist list-group-item text-center list-group-item-action-border-1 px-3' id='lists-user'>";
                        echo '<span><i class="fa fa-search"></i></span>';
                        echo $html->link('Search for..', ['controller'=> 'users', 'action' => 'list']);
                    echo "</div>";
            } else {
                echo "<p class='list-group-item border-1'>No Record found.</p>";
            }
            exit;
        }
    }
    
    public function list()
    {
        $users = $this->paginate($this->Users,['limit' => 7]);

        $this->set(compact('users'));
    }
        
    public function index()
    {
        $user = $this->request->getAttribute('identity');;
        $user_id = $user->id ?? '';
        $tweetTable = $this->loadModel('Tweets');
        $followsTable = $this->loadModel('Follows');
        $retweetTable = $this->loadModel('Retweets');
        $user_image = $user->profile_pic ?? '';

        $retweet_list = $retweetTable->find();
        $retweet_list->select(['tweet_id', 'tweets.user_id', 'user.first_name', 'tweets.content',
                'created' => 'Retweets.created', 'Retweets.user_id', 'retweet_user.first_name', 
                'Retweets.content', 'Retweets.id', 'likes_userid' => 'likes.user_id','Retweets.image',
                'likes_retweetid' => 'likes.retweet_id', 'likes.id', 'retweet_user.profile_pic', 'user.profile_pic'])
            ->join(['table' => 'tweets', 'alias' => 'tweets', 'type' => 'LEFT',
                'conditions' => 'tweets.id = Retweets.tweet_id'])
            ->join(['table' => 'follows', 'alias' => 'Follows', 'type' => 'LEFT',
                'conditions' => 'Retweets.user_id = Follows.following_id'])
            ->join(['table' => 'users', 'alias' => 'user',
                'conditions' => 'tweets.user_id = user.id'])
            ->join(['table' => 'likes', 'alias' => 'likes', 'type' => 'LEFT',
                'conditions' => "likes.tweet_id = tweets.id AND likes.retweet_id = Retweets.id AND likes.user_id = $user_id"])
            ->join(['table' => 'users', 'alias' => 'retweet_user',
                'conditions' => 'Retweets.user_id = retweet_user.id'])
            ->where("Follows.user_id = $user_id AND Retweets.user_id = Follows.following_id AND Retweets.deleted IS NULL
                 OR Retweets.user_id = $user_id AND Retweets.deleted IS NULL");

        $tweet_list = $tweetTable->find();
        $tweet_list->select(['Tweets.id', 'Tweets.user_id', 'user.first_name', 'Tweets.content' ,
            'Tweets.created', 'retweets_userid' => 'NULL', 'retweets_firstname' => 'NULL',
            'retweets_content' => 'NULL', 'retweets_id' => 'NULL', 'likes_userid' => 'likes.user_id','Tweets.image',
            'likes_retweetid' => 'likes.retweet_id', 'likes.id', 'retweet_profile_pic' => 'NULL', 'user.profile_pic'])
            ->join(['table' => 'follows', 'alias' => 'Follows', 'type' => 'LEFT', 
                'conditions' => 'Tweets.user_id = Follows.following_id'])
            ->join(['table' => 'users', 'alias' => 'user', 'type' => 'LEFT', 
                'conditions' => 'Tweets.user_id = user.id'])
            ->join(['table' => 'likes', 'alias' => 'likes', 'type' => 'LEFT', 
                'conditions' => "likes.tweet_id = Tweets.id AND likes.retweet_id IS NULL  AND likes.user_id = $user_id"])
            ->where("Follows.user_id = $user_id AND Tweets.user_id = Follows.following_id AND Tweets.deleted IS NULL 
                OR Tweets.user_id = $user_id AND Tweets.deleted IS NULL");

	    $tweets = $retweet_list->union($tweet_list)->epilog('ORDER BY created DESC');
        $is_tweets = $retweet_list->union($tweet_list)->epilog('ORDER BY created DESC')->count();

        $this->set(compact('tweets','user_image','is_tweets'));
    }
    
    public function logout()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();

            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }

    public function register()
    {
        $this->viewBuilder()->setLayout('users');
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $password = $this->request->getData('password');
            $confirmPassword = $this->request->getData('confirm_password');
            if($password === $confirmPassword){
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user->profile_pic = 'default.png';
                if ($this->Users->save($user)) {
                    $id = $user->id;
                    $message = "
                    <html>
                        <head>
                    <title>HTML email</title>
                    </head>
                    <body style='text-align:center;'>
                    <div style='font-family: Arial, Helvetica, sans-serif;'>
                        <nav style='width:100%; color:white; text-align:center;'><h2>Microblog</h2></nav>
                            <h1>Verify your email address</h1>
                            <p style='text-align: justify; letter-spacing: 3px; text-indent: 50px;'>
                                Hi ".$this->request->getData('first_name').", Please confirm your email address if this is your account. Once it's done you can now use it to log in.
                            </p>
                            <a href='http://mb.cakephp1.ynsdev.pw/users/confirmation/?id=$id'>
                                <button style='background-color:#4CAF50; width: 50%; padding:20px 0; color:#fff; cursor:pointer;'>Verify my email</button>
                            </a>
                        </body>
                    </div>
                    </html>";

                    $mailer = new Mailer('default');
                    $mailer->setEmailFormat('html')
                        ->setFrom(['microblogcakephp4@gmail.com' => 'Microblog'])
                        ->setTo($this->request->getData('email'))
                        ->setSubject('Email Confirmation')
                        ->deliver($message);

                    $this->Flash->success(__('Please confirm your email confirmation to log in.'));
                    
                    return $this->redirect(['action' => 'index']);
                } else{
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('Password did not match'));
            }
        }
        $this->set(compact('user'));
    }

    public function profile($id = null)
    {
        $followsTable = $this->loadModel('Follows');
        $tweetTable = $this->loadModel('Tweets');
        $retweetTable = $this->loadModel('Retweets');
        $user = $this->request->getAttribute('identity');;
        $user_id = $user->id ?? '';
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $following_list = $followsTable->find()
            ->select(['Follows.following_id', 'user.first_name', 'Follows.id', 'user.id'])
            ->join(['table' => 'users', 'alias' => 'user', 'conditions' => 'Follows.following_id = user.id'])
            ->where("Follows.user_id = $id AND following_id != '' ");

        $follower_list = $followsTable->find()
            ->select(['Follows.following_id', 'Follows.user_id', 'user.first_name', 'user.id'])
            ->join(['table' => 'users', 'alias' => 'user', 'conditions' => 'Follows.user_id = user.id'])
            ->where("Follows.following_id = $id ");

        $follower = $followsTable->find()
            ->where("following_id = $id ")
            ->count();

        $following = $followsTable->find()
            ->select(['following_id'])
            ->where("user_id = $id AND following_id != '' ")
            ->count();    

        $isFollow = $followsTable->find()
            ->select(['Follows.id','following_id'])
            ->where("user_id = $user_id AND following_id = $id")
            ->first();   

        $checkfollow = $followsTable->find()
            ->select(['Follows.id','following_id'])
            ->where("user_id = $user_id");
            
        $retweet_list = $retweetTable
            ->find()
            ->select(['tweet_id', 'tweets.user_id', 'user.first_name',
                'tweets.content', 'created' => 'Retweets.created',
                'Retweets.user_id', 'retweet_user.first_name',
                'Retweets.content', 'Retweets.id', 'likes_userid' =>'likes.user_id',
                'likes_retweetid' => 'likes.retweet_id','likes.id','Retweets.image',
                'retweet_user.profile_pic','user.profile_pic'])
            ->join(['table' => 'tweets', 'alias' => 'tweets', 'type' => 'LEFT', 
                'conditions' => 'tweets.id = Retweets.tweet_id'])
            ->join(['table' => 'users', 'alias' => 'user', 
                'conditions' => 'tweets.user_id = user.id'])
            ->join(['table' => 'likes', 'alias' => 'likes', 'type' => 'LEFT', 
                'conditions' => "likes.tweet_id = tweets.id AND likes.retweet_id = Retweets.id AND likes.user_id = $user_id"])
            ->join(['table' => 'users', 'alias' => 'retweet_user', 
                'conditions' => 'Retweets.user_id = retweet_user.id'])
            ->where("Retweets.deleted IS NULL")
            ->where("Retweets.user_id = $id");
            
        $tweet_list = $tweetTable
            ->find()
            ->select(['Tweets.id', 'Tweets.user_id', 'user.first_name',
                'Tweets.content', 'Tweets.created', 'retweets_userid' => 'NULL',
                'retweets_firstname' => 'NULL', 'retweets_content' => 'NULL',
                'retweets_id' => 'NULL', 'likes_userid' => 'likes.user_id',
                'likes_retweetid' => 'likes.retweet_id', 'likes.id','Tweets.image',
                'retweet_profile_pic' => 'NULL', 'user.profile_pic'])
            ->join(['table' => 'users', 'alias' => 'user', 'type' => 'LEFT', 
                'conditions' => 'Tweets.user_id = user.id'])
            ->join(['table' => 'likes', 'alias' => 'likes', 'type' => 'LEFT', 
                'conditions' => "likes.tweet_id = Tweets.id AND likes.retweet_id IS NULL  AND likes.user_id = $user_id"])
            ->where("Tweets.deleted IS NULL")
            ->where("Tweets.user_id = $id");
        
	    $tweets = $retweet_list->union($tweet_list)->epilog('ORDER BY created DESC');
        $is_tweets = $retweet_list->union($tweet_list)->epilog('ORDER BY created DESC')->count();

        $this->set(compact('user','follower','following',
            'tweets','is_tweets','following_list',
            'follower_list','isFollow','id','checkfollow'));
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id);
        $current_userinfo = $this->Authentication->getIdentity()->getOriginalData();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user,$this->request->getData());
            $current_userinfo = $this->Users->patchEntity($current_userinfo,$this->request->getData());

            if(!$user->getErrors){
                $image = $this->request->getData('image_file');
                $file_name = $image->getClientFilename();
                $targetPath = WWW_ROOT.'img'.DS.$file_name;

                if($file_name && !$user->getErrors){
                    $image->moveTo($targetPath);
                    $user->profile_pic = $file_name;
                    $current_userinfo->profile_pic = $file_name;
                }
            }

            if ($this->Users->save($user)) {
                $this->Authentication->setIdentity($current_userinfo);
                $this->Flash->success(__('Successfully updated user info.'));

                return $this->redirect(['action' => "edit/$id"]);
            }
            $this->Flash->error(__('Failed to update user info.'));
        }
        $this->set(compact('user'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
  
    public function confirmation()
    {
       $id = $this->request->getQuery('id');
       if ($id){
        $query = $this->Users->get($id);
        if ($query->verified === null){
            $query->verified = Time::now();
            if ($this->Users->save($query)){
                $this->Flash->success(__('Your email has been confirmed you can now login.'));
                return $this->redirect(['action' => "login"]);
            } else {
                $this->Flash->error(__('Failed confirming email pls try again.'));

                return $this->redirect(['action' => "login"]);
            }
        } else {
            $this->Flash->error(__('Failed You already confirm your email address'));

            return $this->redirect(['action' => "login"]);
            }   
        }
    }
}


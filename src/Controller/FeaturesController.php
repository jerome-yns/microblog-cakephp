<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\Mailer;


class FeaturesController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('users');
    }

    public function reset(){
        if ($this->request->is('post')) {
            $userTable = $this->loadModel('Users');
            $email = $this->request->getQuery('email');
            $findEmail = $userTable->findByEmail($email)->first('password');
            $findEmail = $userTable->patchEntity($findEmail,$this->request->getData());
            if ($userTable->save($findEmail))
            {
                $this->Flash->success(__('Password reset successfully. You can now login.'));

                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            } else{
                $this->Flash->error(__('Failed to reset password.'));
            }
            
           
            return $this->redirect(['controller' => 'features', 'action' => 'reset']);
        }
    }  
    public function find(){
        if ($this->request->is('post')) {
            $message = "
            <html>
                <head>
            <title>HTML email</title>
            </head>
            <body style='text-align:center;'>
            <div style='font-family: Arial, Helvetica, sans-serif;'>
                <nav style='width:100%; color:white; text-align:center;'><h2>Microblog</h2></nav>
                    <h2>Reset Confirmation</h2>
                    <p style='text-align:center; letter-spacing: 3px;'>
                        Click the link to reset the password
                    </p>
                    <a href='http://mb.cakephp1.ynsdev.pw/features/reset/?email=".$this->request->getData('email')."'>
                        <button style='background-color:#4CAF50; width: 50%; padding:20px 0; color:#fff; cursor:pointer;'>Reset Password</button>
                    </a>
                </body>
            </div>
            </html>";

            $mailer = new Mailer('default');
            $mailer->setEmailFormat('html')
                ->setFrom(['microblogcakephp4@gmail.com' => 'Microblog'])
                ->setTo($this->request->getData('email'))
                ->setSubject('Reset Password')
                ->deliver($message);

            $this->Flash->success(__('Reset link has been sent to your email.'));

            return $this->redirect(['action' => 'find']);
        }  
    }
    public function cancel(){
        if ($this->request->is('post')) {
            return $this->redirect(['controller' => 'users', 'action' => 'register']);
        }
    }


}
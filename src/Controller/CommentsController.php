<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\Time;

/**
 * Comments Controller
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * Filter
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('Homepage');

        $user = $this->request->getAttribute('identity');
        $name = $user->first_name ?? '';
        $user_id = $user->id ?? '';

        $this->set(compact('name', 'user_id'));
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $tweetId = $this->request->getQuery('tweet');
        $TweetTable = $this->loadModel('Tweets');
        $UserTable = $this->loadModel('Users');
        
        $tweet = $TweetTable->get($tweetId, [
            'contain' => [],
        ]);

        $tweet_userid = $tweet->user_id;

        $user = $UserTable->get($tweet_userid, [
            'contain' => [],
        ]);

         $tweets = $TweetTable->find()
            ->select(['user.first_name','user.id','tweets.content',
                'tweets.created', 'tweets.user_id','tweets.id',
                'comment.content','comment.created', 'comment.id'])
            ->distinct(['user.first_name','user.id','tweets.content',
                'tweets.created', 'tweets.user_id', 'tweets.id',
                'comment.content', 'comment.created', 'comment.id'])
            ->join(['table' => 'comments', 'alias' => 'comment', 
                'conditions' => 'tweets.id = comment.tweet_id'])
            ->join(['table' => 'users', 'alias' => 'user', 
                'conditions' => 'comment.user_id = user.id'])
            ->where("comment.tweet_id = $tweetId")
            ->where("comment.deleted IS NULL")
            ->order(['tweets.created' => 'DESC']);

        $this->set(compact('tweets','tweet','user','tweetId'));
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tweet_id = $id;
        $TweetTable = $this->loadModel('Tweets');
        $UserTable = $this->loadModel('Users');
        
        $tweet = $TweetTable->get($tweet_id, [
            'contain' => [],
        ]);

        $tweet_userid = $tweet->user_id;

        $user = $UserTable->get($tweet_userid, [
            'contain' => [],
        ]);

         $tweets = $TweetTable->find()
            ->select(['user.first_name','user.profile_pic','user.id', 'Tweets.content',
                'Tweets.created', 'Tweets.user_id', 'Tweets.id',
                'comment.content', 'comment.created', 'comment.id', 'comment.user_id'])
            ->distinct(['user.first_name','user.profile_pic','user.id','Tweets.content',
                'Tweets.created', 'Tweets.user_id', 'Tweets.id',
                'comment.content', 'comment.created', 'comment.id', 'comment.user_id'])
            ->join(['table' => 'comments', 'alias' => 'comment', 
                'conditions' => 'Tweets.id = comment.tweet_id'])
            ->join(['table' => 'users', 'alias' => 'user', 
                'conditions' => 'comment.user_id = user.id'])
            ->where("comment.tweet_id = $id")
            ->where("retweet_id IS NULL")
            ->where("comment.deleted IS NULL")
            ->order(['Tweets.created' => 'DESC']);

        $this->set(compact('tweets','tweet','user','tweet_id'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null, $retweet_id = null)
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->id ?? '';

        $comment = $this->Comments->newEmptyEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $comment->user_id = $user_id;
            $comment->tweet_id = $id;
            $comment->retweet_id = $retweet_id;
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));

                if ($retweet_id){
                    return  $this->redirect("/retweets/view/$retweet_id");;
                }

                return  $this->redirect("/comments/view/$id");;
            }
            if($retweet_id){
                $this->Flash->error(__('The comment could not be posted'));
                return  $this->redirect("/retweets/view/$retweet_id");
            } else {
                $this->Flash->error(__('The comment could not be posted'));
                return  $this->redirect("/comments/view/$id");
            }
           
        }
        $this->set(compact('comment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $this->set(compact('comment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $tweet_id = null, $retweet_id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $comment = $this->Comments->get($id);

        $comment->deleted = Time::now();
        if ($this->Comments->save($comment)){
            $this->Flash->success(__('The retweet has been deleted.'));
        } else {
            $this->Flash->error(__('The retweet could not be deleted. Please, try again.'));
        }
        if($retweet_id){
                return  $this->redirect("/retweets/view/$retweet_id");;
        }

        return  $this->redirect("/comments/view/$tweet_id");
    }
}

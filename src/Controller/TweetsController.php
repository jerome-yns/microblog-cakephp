<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\I18n\Time;
use Cake\View\Helper\HtmlHelper; 

/**
 * Tweets Controller
 *
 * @property \App\Model\Table\TweetsTable $Tweets
 * @method \App\Model\Entity\Tweet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TweetsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

     
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('Homepage');
    }
    
    public function search()
    {
        $html = new HtmlHelper(new \Cake\View\View());
        if($this->request->getData('query')){
            $inputText = $this->request->getData('query');
            $result = $this->Tweets->find()
                ->where(['content LIKE' =>'%'.$inputText.'%'])
                ->where(['deleted IS NULL'])
                ->all();

            if ($result->count()>0){
                foreach ($result as $res) {
                    echo "<div class='d-flex align-items-center list-group-item list-group-item-action-border-1 px-3' id='lists-user'>";
                        echo "Tweets:";
                        echo $html->link(h(ucwords($res->content)), ['controller'=> 'comments', 'action' => 'view', $res->id]);
                    echo "</div>";
                }
            } else {
                echo "<p class='list-group-item border-1'>No Record found.</p>";
            }
            exit;
        }
    }

    public function index()
    {
        $user = $this->request->getAttribute('identity');
        $current_userid = $user->id ?? '';
        $FollowsTable = $this->loadModel('Follows');

        $this->paginate = [
            'contain' => ['Users', 'Comments', 'Likes.Users', 'Retweets.Users','Follows'],
        ];

        $tweets = $this->paginate($this->Tweets);
        
        $this->set(compact('tweets','current_userid'));
    }

    /**
     * View method
     *
     * @param string|null $id Tweet id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tweet = $this->Tweets->get($id, [
            'contain' => ['Users', 'Comments', 'Likes', 'Retweet'],
        ]);

        $this->set(compact('tweet'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->request->getAttribute('identity');
        $user_id = $user->id ?? '';
       

        $tweet = $this->Tweets->newEmptyEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tweet = $this->Tweets->patchEntity($tweet, $this->request->getData());
            $tweet->user_id = $user_id;

            if (!$tweet->getErrors){
                $image = $this->request->getData('image_file');
                $file_name = $image->getClientFilename();
                $targetPath = WWW_ROOT.'img'.DS.$file_name;

                if ($file_name && !$tweet->getErrors){
                    $image->moveTo($targetPath);
                    $tweet->image = $file_name;
                    
                }
            }

            if ($this->Tweets->save($tweet)) {
                $this->Flash->success(__('The tweet has been saved.'));
                
                return $this->redirect(['controller' => 'users','action' => 'index']);
            }
            $this->Flash->error(__('The tweet could not be posted'));

            return $this->redirect(['controller' => 'users','action' => 'index']);
        }
        $this->set(compact('tweet', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tweet id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tweet = $this->Tweets->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tweet = $this->Tweets->patchEntity($tweet, $this->request->getData());
            if ($this->Tweets->save($tweet)) {
                $this->Flash->success(__('The tweet has been updated.'));

                return $this->redirect(['controller' => 'users','action' => 'index']);
            }
            $this->Flash->error(__('The tweet could not be updated. Please, try again.'));

            return $this->redirect(['controller' => 'users','action' => 'index']);
        }
        $users = $this->Tweets->Users->find('list', ['limit' => 200]);
        $this->set(compact('tweet', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tweet id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);
        $tweet = $this->Tweets->get($id);

        $tweet->deleted = Time::now();
        if ($this->Tweets->save($tweet)){
            $this->Flash->success(__('The tweet has been deleted.'));
        } else {
            $this->Flash->error(__('The tweet could not be deleted. Please, try again.'));
        }
        
        return $this->redirect(['controller' => 'users', 'action' => 'index']);
    }
}

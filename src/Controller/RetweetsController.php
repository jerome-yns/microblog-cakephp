<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\I18n\Time;
use Cake\View\Helper\HtmlHelper; 

/**
 * Retweets Controller
 *
 * @method \App\Model\Entity\Retweet[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RetweetsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('Homepage');

        $user = $this->request->getAttribute('identity');
        $name = $user->first_name ?? '';
        $user_id = $user->id ?? '';

        $this->set(compact('name','user_id'));
    }

    public function index()
    {
        $retweets = $this->paginate($this->Retweets);

        $this->set(compact('retweets'));
    }

    public function search(){
        $html = new HtmlHelper(new \Cake\View\View());
        if ($this->request->getData('query')){
            $inputText = $this->request->getData('query');
            $result = $this->Retweets->find()
                ->where(['content LIKE' =>'%'.$inputText.'%'])
                ->where(['deleted IS NULL'])
                ->all();

            if ($result->count()>0){
                foreach ($result as $res) {
                    echo "<div class='d-flex align-items-center list-group-item list-group-item-action-border-1 px-3' id='lists-user'>";
                        echo "Retweets:";
                        echo $html->link(h(ucwords($res->content)), ['controller'=> 'retweets', 'action' => 'view', $res->id]);
                    echo "</div>";
                }
            } else {
                echo "<p class='list-group-item border-1'>No Record found.</p>";
            }
            exit;
        }
    }

    /**
     * View method
     *
     * @param string|null $id Retweet id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $retweet = $this->Retweets->get($id, [
            'contain' => [],
        ]);

        $user = $this->request->getAttribute('identity');;
        $user_id = $user->id ?? '';
        $tweetTable = $this->loadModel('Tweets');
        $retweetTable = $this->loadModel('Retweets');

        $tweets = $retweetTable->find()
            ->select(['tweet_id','Tweets.user_id', 'user.first_name', 'user.profile_pic',
                'Tweets.content', 'created' => 'Retweets.created', 'Retweets.user_id','Tweets.image',
                'retweet_user.first_name', 'retweet_user.profile_pic', 'Retweets.content', 'Retweets.id'])
            ->join(['table' => 'tweets', 'alias' => 'Tweets', 'type' => 'LEFT', 
                'conditions' => 'Tweets.id = Retweets.tweet_id'])
            ->join(['table' => 'users', 'alias' => 'user', 
                'conditions' => 'Tweets.user_id = user.id'])
            ->join(['table' => 'users', 'alias' => 'retweet_user', 
                'conditions' => 'Retweets.user_id = retweet_user.id'])
            ->WHERE("Retweets.id = $id");

        $comments = $tweetTable->find()
            ->select(['user.first_name','user.profile_pic','user.id','Tweets.content',
                'Tweets.created', 'Tweets.user_id','Tweets.id',
                'comment.content', 'comment.created','comment.id',
                'comment.user_id', 'comment.retweet_id'])
            ->distinct(['user.first_name','user.id','Tweets.content',
                'Tweets.created', 'Tweets.user_id', 'Tweets.id',
                'comment.content', 'comment.created', 'comment.id',
                'comment.user_id', 'comment.retweet_id'])
            ->join(['table' => 'comments', 'alias' => 'comment', 
                'conditions' => 'Tweets.id = comment.tweet_id'])
            ->join(['table' => 'users', 'alias' => 'user', 
                'conditions' => 'comment.user_id = user.id'])
            ->WHERE("comment.retweet_id = $id")
            ->WHERE("retweet_id IS NOT NULL")
            ->WHERE("comment.deleted IS NULL")
            ->order(['Tweets.created' => 'DESC']);
            
        $this->set(compact('tweets','comments'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($id,$image = null)
    {
        
        $user = $this->request->getAttribute('identity');
        $user_id = $user->id ?? '';

        $retweet = $this->Retweets->newEmptyEntity();
        if ($this->request->is('post')) {           
            $retweet = $this->Retweets->patchEntity($retweet, $this->request->getData());
            $retweet->tweet_id = $id;
            $retweet->user_id = $user_id;
            if ($image){
                $retweet->image = $image;
            }
            
            
            if ($this->Retweets->save($retweet)) {
                $this->Flash->success(__('The retweet has been saved.'));

                return $this->redirect(['controller' => 'users', 'action' => 'index']);
            }
            $this->Flash->error(__('The retweet post could not be posted'));
            
            return $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        $this->set(compact('retweet'));
    }
    
    /**
     * Edit method
     *
     * @param string|null $id Retweet id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $retweet = $this->Retweets->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $retweet = $this->Retweets->patchEntity($retweet, $this->request->getData());
            if ($this->Retweets->save($retweet)) {
                $this->Flash->success(__('The retweet has been saved.'));

                return $this->redirect(['controller' => 'users', 'action' => 'index']);
            }
            $this->Flash->error(__('The retweet could not be saved. Please, try again.'));
        }
        $this->set(compact('retweet'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Retweet id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete','get']);

        $retweet = $this->Retweets->get($id);

        $retweet->deleted = Time::now();
        if ($this->Retweets->save($retweet)){
            $this->Flash->success(__('The retweet has been deleted.'));
        } else {
            $this->Flash->error(__('The retweet could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'users', 'action' => 'index']);
    }
}

<div class="card">
    <div class="card-body w-75 m-auto" >
        <div class="d-flex">
            <div class="profile-link">
                <a href="<?= $this->Url->build(['controller'=>'users',
                                'action'=>'profile',
                                $tweet->tweets['user_id'],]) ?>">                                    
                    <?= $this->Html->image($tweet->user['profile_pic'],['class' => 'profile_pic']) ?>
                    <?= ucwords($tweet->user['first_name']) ?>
                    <small class="text-muted">&#8226; <?= h(get_timediff($tweet->created) ) ?> </small> 
                </a>
            </div>
            <?php if($user_id == $tweet->tweets['user_id']): ?>
                <div class="dropdown ml-auto">
                    <i id="dropdownMenuButton" class="post-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                    <div class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#tweetModal<?= h($tweet->tweet_id)?>">Edit Post</a>
                        <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'tweets',
                            'action'=>'delete',
                            $tweet->tweet_id,]) ?>">Delete Post
                        </a>
                    </div>
                </div>
            <?php endif; ?>
        </div>        
        <a href="<?=$this->Url->build([
                'controller' => 'comments',
                'action' => 'view',
                $tweet->tweet_id,
            ]);?>">
            <div class="card w-75 my-5" id="tweet-content">
                <div class="card-body">
                    <?php if ($tweet->image): ?>
                        <div class="card-body tweet-content">
                            <p class="card-text text-left"><?= h($tweet->tweets['content']) ?></p>
                        </div>
                        <?= $this->Html->image($tweet->image,['class' => 'card-img-bottom']) ?>
                    <?php else: ?>
                        <p class="card-text"><?= h($tweet->tweets['content']) ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </a>
        <div class="tweets-btn">
            <?php if ($tweet->likes_userid){
                echo "<i class='fa fa-thumbs-up text-primary'>";                       
                echo $this->Form->postLink('like', [
                'controller' => 'likes',
                'action' => 'delete', 
                'class' => 'btn btn-warning fa fa-envelope',
                'id' => 'likebtn',
                $tweet->likes['id']]);
            } else{
                echo " <i class='fa fa-thumbs-up text-secondary'> ";
                echo $this->Form->postLink('like', [
                'controller' => 'likes',
                'action' => 'add', 
                'class' => 'btn btn-warning fa fa-envelope',
                'id' => 'likebtn',
                $tweet->tweet_id]);
            }  
            ?>                     
            </i>
            <a class="fa fa-comment" href="<?=$this->Url->build([
                'controller' => 'comments',
                'action' => 'view',
                $tweet->tweet_id,
            ]);?>"> Comment</a>
            <a class="fa fa-retweet" data-toggle="modal" data-target="#retweetModal<?= h($tweet->tweet_id) ?>"> Retweet</a> 
        </div>
    </div>
</div>
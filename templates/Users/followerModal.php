<div class="modal fade" id="followerModal<?= h($user->id) ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header ">
                    <h4 class="modal-title" id="exampleModalLongTitle">Followers List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <ul id="follower-list" class="container" >
                <?php foreach ($follower_list as $key => $follower): ?>
                    <div class="d-flex justify-content-between align-items-center">
                        <li><?= h($follower->user['first_name']) ?></li>
                        <?= $this->Form->postButton('View Profile', [
                            'controller' => 'users',
                            'action' => 'profile', 
                            $follower->user['id']]);
                        ?>
                    </div>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
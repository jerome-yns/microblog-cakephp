<?= $this->Html->css('style') ?>
<div class="container-fluid validation-container">
    <div class="row h-100 validation-content">
        <div class="d-flex justify-content-center align-items-center" id="left-div">
        </div>
        <div class="d-flex justify-content-center align-items-center" id="right-div">
            <div class="column-responsive column-80">
                 <h1 class="text-center">Login</h1>
                 <?= $this->Form->create() ?>
                 <fieldset>
                    <?php
                        echo $this->Form->control('email', array( 'label' => false, 'placeholder' => 'Email', 'required' => true, 'autocomplete' => 'off'));
                        echo $this->Form->control('password', array( 'label' => false, 'placeholder' => 'Password', 'required' => true));
                    ?>
                </fieldset>
                Don't have an account?<a href='<?= $this->Url->build(['controller'=>'users','action' => 'register']) ?>' class="text-primary"> Create an account</a><br>
                <a href='<?= $this->Url->build(['controller'=>'features','action' => 'find']) ?>' class="text-primary">Forgot Password?</a><br><br>
                <div class="text-center">
                    <?= $this->Form->submit(__('Login')) ?> 
                </div>  
                 <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

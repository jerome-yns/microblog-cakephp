
<div class="modal fade" id="retweetModal<?= h($tweet->tweet_id) ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLongTitle">Retweet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body input-modal">
                <?= $this->Form->create(null, [
                        'url' => [
                            'controller' => 'retweets',
                            'action' => 'add',
                            $tweet->tweet_id,$tweet->image
                        ]]) ?>
                        <?= $this->Form->control('content',['label' => false, 'placeholder' => "What's on your mind?", 'autocomplete' => 'off', 'maxlength'=>'140', 'required' => true]) ?>
                        <div class="card">
                            <div class="card-body w-100 text-left">
                                <div class="d-flex align-items-center">
                                    <?php if ($tweet->user['profile_pic']): ?>
                                        <?= $this->Html->image($tweet->user['profile_pic'],['class' => 'profile_pic']) ?>
                                    <?php else: ?>
                                        <?= $this->Html->image('profile.png',['class' => 'profile_pic']) ?>
                                    <?php endif; ?>
                                    <div class="ml-1">
                                        <?= h(ucwords($tweet->user['first_name'])) ?>
                                    </div>
                                </div>
                            <div class="card">
                                <div class="card-body retweet-modal">
                                    <?php if ($tweet->image): ?>
                                        <div class="card-body">
                                            <p class="card-text text-left"><?= h($tweet->tweets['content']) ?></p>
                                        </div>
                                        <?= $this->Html->image($tweet->image,['class' => 'card-img-bottom']) ?>
                                    <?php else: ?>
                                        <p class="card-text"><?= h($tweet->tweets['content']) ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->submit(_('Retweet')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div> 
<?php include 'timediff.php' ?>
<div class="container container-sm">
    <div class="card">
        <div class="card-body d-flex justify-content-center" id="post-content">
            <span>
                <?= $this->Html->image($user_image,['class' => 'post_userimage']) ?>
            </span>
            <?= $this->Form->create(null, [ 'type' => 'file',
                'url' => [
                    'controller' => 'tweets',
                    'action' => 'add',
                ],]) ?>
                <?= $this->Form->control('content', ['label' => false, 'placeholder' => "What's on your mind?", 'maxlength'=>'140', 'required' => true, 'autocomplete' => 'off' ]) ?>
                <label>
                    <i class="fa fa-image"></i>
                        <?= $this->Form->control('image_file', ['type' => 'file','label' => false, 'style' => 'display:none;', 'id' => 'tweet-post'])?>
                    <p id="file-name"></p>
                </label>
                <?= $this->Form->submit(_('Tweet')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <?php if ($is_tweets === 0): ?>
        <div class="card w-100 my-5" id="tweet-content">
            <div class="card-body">
                <p class="card-text">This Person is Lazy to post something.</p>
            </div>
        </div>
    <?php endif; ?>
    <?php foreach ($tweets as $tweet): ?>
        <?php $tweet->user_id ? include 'retweet.php' :  include 'content.php' ?>
        <?php include 'updateModal.php' ?>
        <?php include 'retweetModal.php' ?>  
    <?php endforeach; ?>
</div>
<script>
    $(document).ready(function() {
        $('#tweet-post').change(function(e) {
            var filename = e.target.files[0].name;
            $("#file-name").text(filename);
        });
    });
</script>
<?= $this->Html->css('style') ?>
<div class="container-fluid validation-container">
    <div class="row h-100 validation-content">
        <div class="d-flex justify-content-center align-items-center" id="left-div">
        </div>
        <div class=" d-flex justify-content-center align-items-center" id="right-div">
            <div class="column-responsive column-80">
                 <h1 class="text-center">Sign Up</h1>
                 <?= $this->Form->create($user) ?>
                    <?php
                        echo $this->Form->control('username', ['label' => false, "placeholder" => "Username", 'autocomplete' => 'off']);
                        echo $this->Form->control('first_name', ['label' => false, "placeholder" => "First Name", 'autocomplete' => 'off']);
                        echo $this->Form->control('last_name', ['label' => false, "placeholder" => "Last Name", 'autocomplete' => 'off']);
                        echo $this->Form->control('email', ['label' => false, "placeholder" => "Email", 'autocomplete' => 'off']);
                        echo $this->Form->control('password', ['label' => false, "placeholder" => "Password"]);
                        echo $this->Form->control('confirm_password', ['type' => 'password','label' => false, "placeholder" => "Confirm Password"]);
                    ?>
                    <a href='<?= $this->Url->build(['controller'=>'users','action' => 'login']) ?>' class="text-primary">Already have an account?</a>
                    <div class="text-center">
                        <?= $this->Form->button(__('Submit')) ?> 
                    </div>  
                 <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

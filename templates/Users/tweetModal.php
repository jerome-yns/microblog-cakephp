<div class="modal fade" id="tweetModal<?= h($user->id) ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLongTitle">Post Tweet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body input-modal">
                <?= $this->Form->create(null, [ 'type' => 'file',
                    'url' => [
                        'controller' => 'tweets',
                        'action' => 'add',
                    ],]) ?>
                    <?= $this->Form->control('content', ['label' => false, 'placeholder' => "What's on your mind?", 'maxlength'=>'140', 'required' => true, 'autocomplete' => 'off' ]) ?>
                    <label>
                        <i class="fa fa-image"></i>
                            <?= $this->Form->control('image_file',['type' => 'file','label' => false, 'style' => 'display:none;', 'id' => 'tweet-post'])?>
                        <p id="file-name"></p>
                    </label>
                    <?= $this->Form->submit(_('Tweet')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#tweet-post').change(function(e) {
            var filename = e.target.files[0].name;
            $("#file-name").text(filename);
        });
    });
</script>
<div class="container container-sm">
<div class="column-responsive column-80">
        <div class="form content ">
            <?= $this->Form->create($user,['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Edit Profile',['required' => false]) ?></legend>
                <div class="row edit-page">
                    <div class="col-4 dp"> 
                        <?= $this->Html->image($user->profile_pic,['class' => 'nav_userimage']) ?>
                        <?= $this->Form->control('image_file',['type' => 'file' ,'label' => 'Change image'])?>
                    </div>
                    <div class="col-8">
                        <?php
                            echo $this->Form->control('username', ['label' => false , 'autocomplete' => 'off']);
                            echo $this->Form->control('first_name', ['label' => false, 'autocomplete' => 'off']);
                            echo $this->Form->control('last_name', ['label' => false, 'autocomplete' => 'off']);
                            echo $this->Form->control('email', ['label' => false, 'autocomplete' => 'off']);
                            echo $this->Form->control('password',['type' => 'password', 'label' => false]);
                        ?>
                    </div>
                </div>
            </fieldset>
            <?= $this->Form->button(__('Update')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

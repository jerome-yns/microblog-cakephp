<div class="card">
    <div class="card-body w-75 m-auto">
        <div class="d-flex">
            <div class="profile-link">
                <a href="<?= $this->Url->build(['controller'=>'users',
                    'action'=>'profile',
                    $tweet->user_id,]) ?>"> 
                    <?= $this->Html->image($tweet->retweet_user['profile_pic'] ,['class' => 'profile_pic']) ?>
                    <?= h(ucwords($tweet->retweet_user['first_name'])) ?>
                    <small class="text-muted">&#8226; <?= h(get_timediff($tweet->created)) ?> </small></i>
                </a>
            </div>
            <?php if ($user_id == $tweet->user_id): ?>
            <div class="dropdown ml-auto">
                    <i id="dropdownMenuButton" class="post-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                    <div class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="" data-toggle="modal" data-target="#retweetUpdateModal<?= h($tweet->id) ?>">Edit Post</a>
                    <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'retweets',
                        'action'=>'delete',
                        $tweet->id]) ?>">Delete Post
                    </a>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <p class="text-left pl-5 my-2"> <i class="fa fa-retweet mt-10"></i>
            <?= h($tweet->content) ?>
        </p>
        <div class="w-75 m-auto retweet-content">
            <a href="<?= $this->Url->build(['controller'=>'users',
                            'action'=>'profile',
                            $tweet->tweets['user_id']]) ?>"> 
                    <?php if ($tweet->user['profile_pic']): ?>
                        <?= $this->Html->image($tweet->user['profile_pic'],['class' => 'profile_pic']) ?>
                        <?php else: ?>
                        <?= $this->Html->image('profile.png',['class' => 'profile_pic']) ?>
                    <?php endif; ?> 
                    <?= h(ucwords($tweet->user['first_name'])) ?>
                    <small class="text-muted">&#8226; <?= h(get_timediff($tweet->created)) ?> </small>
                </i>
            </a>
            <a  href="<?=$this->Url->build([
                'controller' => 'comments',
                'action' => 'view',
                $tweet->tweet_id
            ]);?>">
                <div class="card mt-3">
                    <div class="card-body m-auto">
                        <?php if ($tweet->image): ?>
                            <div class="card-body tweet-content">
                                <p class="card-text text-left"><?= h($tweet->tweets['content']) ?></p>
                            </div>
                            <?= $this->Html->image($tweet->image,['class' => 'card-img-bottom']) ?>
                        <?php else: ?>
                            <p class="card-text"><?= h($tweet->tweets['content']) ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </a>
        </div><br/>
        <div class="tweets-btn">
            <?php if ($tweet->likes_userid){
                echo "<i class='fa fa-thumbs-up text-primary'>";
                echo $this->Form->postLink('like', [
                'controller' => 'likes',
                'action' => 'delete', 
                'class' => 'btn btn-warning fa fa-envelope',
                'id' => 'likebtn',
                $tweet->likes['id'],$tweet->id]);
            } else {
                echo " <i class='fa fa-thumbs-up text-secondary'> ";
                echo $this->Form->postLink('like', [
                    'controller' => 'likes',
                    'action' => 'add', 
                    'class' => 'btn btn-warning fa fa-envelope',
                    'id' => 'likebtn',
                    $tweet->tweet_id, $tweet->id]);
            } ?>
            </i>
            <a class="fa fa-comment" href="<?=$this->Url->build([
                'controller' => 'retweets',
                'action' => 'view',
                $tweet->id,
            ]);?>"> Comment</a>
            <a class="fa fa-retweet" data-toggle="modal" data-target="#reretweetModal<?= h($tweet->id) ?>"> Retweet</a> 
        </div>
    </div>
</div>
<?php include 'retweetedModal.php' ?>
<?php include 'retweetUpdateModal.php' ?>
<?php include 'retweetModal.php' ?>  
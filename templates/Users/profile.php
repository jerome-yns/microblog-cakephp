<?php include 'timediff.php' ?>
<div class="container container-sm">
    <div class="card">
        <div class="card-body d-flex" id="post-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm dp">
                            <?= $this->Html->image($user->profile_pic,['class' => 'nav_userimage']) ?>
                    </div>
                    <div class="col-sm mt-1">
                        <h5><?= h($user->first_name.' '.$user->last_name) ?></h5>
                        <h5><?= h($user->username) ?></h5>
                        <h5>Joined: <?= h(date("Y-m",strtotime($user->created))) ?></h5>
                        <div class="d-flex justify-content-between">
                            <a data-toggle="modal" class="follow-list" data-target="#followingModal<?= h($user->id) ?>">Following: <?= h($following) ?></a>  
                            <a data-toggle="modal" class="follow-list" data-target="#followerModal<?= h($user->id) ?>">Follower: <?= h($follower) ?></a>  
                        </div>
                    </div>
                    <div class="col-sm flex-column d-flex align-items-end justify-content-between">
                        <?php if ($user->id == $user_id): ?>
                            <div class="dropdown">
                                <i class="fa fa-cog" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item text-center" href="<?= $this->Url->build(['controller'=>'users',
                                            'action'=>'edit',
                                            $user->id]) ?>">Edit Profile
                                        </a>
                                    <!-- <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'tweets',
                                        'action'=>'delete',
                                        $user->id]) ?>">Settings
                                    </a> -->
                                </div>
                            </div>
                            <div>
                                <button  data-toggle="modal" data-target="#tweetModal<?= h($user->id) ?>">Tweet</button>
                            </div>
                            <?php else: ?>
                            <div></div>
                            <?php if ($isFollow){
                                echo $this->Form->postButton('Unfollow', [
                                'controller' => 'follows',
                                'action' => 'delete', 
                                $isFollow->id,$id]);
                            } else {
                                echo $this->Form->postButton('Follow', [
                                'controller' => 'follows',
                                'action' => 'add', 
                                $id]);
                            }  
                                ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4>Tweets</h4>
    </div>
</div>
<div class="container container-sm">
    <?php if($is_tweets === 0): ?>
        <div class="card w-100 my-5" id="tweet-content">
            <div class="card-body">
                <p class="card-text">This Person is Lazy to post something.</p>
            </div>
        </div>
    <?php endif; ?>
    <?php foreach ($tweets as $tweet): ?>
        <?php $tweet->user_id ? include 'retweet.php' :  include 'content.php' ?>
        <?php include 'updateModal.php' ?>
        <?php include 'retweetModal.php' ?>   
        <?php include 'followingModal.php' ?>  
        <?php include 'followerModal.php' ?>  
    <?php endforeach; ?>
    <?php include 'tweetModal.php' ?> 
</div>


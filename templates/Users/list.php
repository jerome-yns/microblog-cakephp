   <div class="container mt-5"> 
    <?php if ($name): ?>
        <div class="p-0 m-0">
          <?= $this->Form->create(null, [
                  'url' => [
                      'controller' => 'Users',
                      'action' => 'search',
                  ],'type' => 'get']) ?>
                <div class="d-flex align-items-center justify-content-center">
                <?= $this->Form->control('key',['id' => 'searchlist','autocomplete' => 'off', 'label'=> false, 'placeholder' => 'Search']); ?>
            <?= $this->Form->end(); ?>
        </div>
        <div class="text-center">
        <select class="p-0 m-0" id="filter" style="color:#000; width:20%;">
                      <option value="users" selected>Search:Users</option>
                      <option value="tweets">Search:Tweets</option>
                      <option value="retweets">Search:Retweets</option>
                </select>
        </div>
      <?php endif; ?>
      <div class=" m-auto">
  <div class="list-group mt-5" id="showlist">
        <?php foreach ($users as $key => $user): ?>
        <div class='d-flex align-items-center list-group-item list-group-item-action-border-1 px-3' id='lists-user'>
                    <?php if ($user->profile_pic): ?> 
                        <?= $this->Html->image($user->profile_pic,['class' => 'nav_userimage']) ?>
                    <?php endif; ?> 
                    <?= $this->Html->link(h(ucwords($user->first_name.' '.$user->last_name)), ['controller'=> 'users', 'action' => 'profile', $user->id]) ?>
            </div>
        <?php endforeach; ?>
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('select').click(function(){
        $('#searchlist').val('');
    });
    $('#searchlist').keyup(function(){
      if (!searchText){
        var e = document.getElementById("filter");
        var options = e.value;
        var searchText = $(this).val();
        var url;
        if (options === 'users') {
          url = "<?= $this->Url->build(['controller' => 'users', 'action' => 'search']) ?>";
        } else if  (options === 'tweets') {
          url = "<?= $this->Url->build(['controller' => 'tweets', 'action' => 'search']) ?>";
        } else if  (options === 'retweets'){
          url = "<?= $this->Url->build(['controller' => 'retweets', 'action' => 'search']) ?>";
        }
        $.ajax({
          url: url,
          method:'post',
          data:{query:searchText},
          headers:{
              'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
          },
          success:function(response){
            $('#showlist').html(response);
          }
        })
      }
      else{
        $('#showlist').html('');
      }
      
    });
  })
</script>
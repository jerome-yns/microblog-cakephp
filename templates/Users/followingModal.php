<div class="modal fade" id="followingModal<?= h($user->id) ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header ">
                    <h4 class="modal-title" id="exampleModalLongTitle">Following List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <ul id="follower-list" class="container">
                <?php foreach ($following_list as $key => $following): ?>
                    <div class="d-flex justify-content-between align-items-center">
                        <li><?= h($following->user['first_name']) ?></li>
                        <?= $this->Form->postButton('View Profile', [
                            'controller' => 'users',
                            'action' => 'profile', 
                            $following->user['id']]);
                        ?>
                    </div>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
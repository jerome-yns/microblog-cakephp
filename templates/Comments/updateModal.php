<div class="modal fade" id="tweetModal<?= $tweet->tweet_id ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLongTitle">Edit Tweet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null, [
                        'url' => [
                            'controller' => 'tweets',
                            'action' => 'edit',
                            $tweet->tweet_id
                        ]]) ?>
                    <?= $this->Form->control('content',['label' => false, 'value' => h($tweet->tweets['content']), 'maxlength'=>'140', 'required' => true]) ?>
                    <?= $this->Form->submit(_('Update tweet')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?php include 'timediff.php' ?>
<div class="container container-sm">
    <div class="card">
        <div class="card-body d-flex justify-content-center" id="post-content">
        <span><i class="fa fa-user-circle fa-2x"id="profile-icon"></i></span>
            <?= $this->Form->create(null, [
                'url' => [
                    'controller' => 'tweets',
                    'action' => 'add'
                ]]) ?>
                <?= $this->Form->control('content',['label' => false, 'placeholder' => "What's on your mind?", 'maxlength'=>'140', 'required' => true]) ?>
                <?= $this->Form->submit(_('Tweet')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
    
    <div class="container container-sm">
    <?php foreach ($tweets as $tweet): ?>
        <div class="card">
            <div class="card-body w-75 m-auto">
                <a href="<?= $this->Url->build(['controller'=>'users',
                                'action'=>'profile',
                                $tweet->tweets['id'],]) ?>"> 
                    <i class="fa fa-user-circle fa-lg">
                        <?= h(ucwords($tweet->user['first_name'])) ?>
                        <small class="text-muted">&#8226; <?= h(get_timediff($tweet->tweets['created'])) ?> </small>
                    </i>
                </a>
                <div class="card mt-3">
                <div class="card-body m-auto">
                    <?php if ($user_id == $tweet->tweets['user_id']): ?>
                        <div class="dropdown .retweet-menu">
                            <i id="dropdownMenuButton" class="post-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="" data-toggle="modal" data-target="#tweetModal<?= h($tweet->tweets['id']) ?>">Edit Post</a>
                                <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'tweets',
                                    'action'=>'delete',
                                    $tweet->tweets['id'],]) ?>">Delete Post
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <p class="card-text"><?= h($tweet->tweets['content']) ?></p>
                    </div>
                </div>
                <div class="tweets-btn">
                    <i class="fa fa-thumbs-up"> Likes</i> 
                    <a class="fa fa-comment" href="<?=$this->Url->build([
                        'controller' => 'comments',
                        'action' => 'view',
                        $tweet->tweets['id'],
                    ]);?>"> Comment</a>
                    <i class="fa fa-retweet"> Retweet</i> 
                </div>
            </div>
        </div>
        <div class="modal fade" id="tweetModal<?= h($tweet->tweets['id']) ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header ">
                        <h4 class="modal-title" id="exampleModalLongTitle">Edit Tweet</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?= $this->Form->create(null, [
                                'url' => [
                                    'controller' => 'tweets',
                                    'action' => 'edit',
                                    $tweet->tweets['id']
                                ]]) ?>
                            <?= $this->Form->control('content',['label' => false, 'value' => h($tweet->tweets['content']), 'maxlength'=>'140', 'required' => true]) ?>
                            <?= $this->Form->submit(_('Update tweet')) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>  
    <?php endforeach; ?>
    </div>
</div>

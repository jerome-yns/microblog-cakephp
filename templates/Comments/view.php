<style>
    a,a:hover,a:active{
        text-decoration:none;
        color:inherit;
    }
</style>
<?php include 'timediff.php' ?>
  <div class="container container-sm">
        <div class="card">
            <div class="card-body w-75" style="margin:auto;">
                <a href="<?= $this->Url->build(['controller'=>'users',
                                'action'=>'profile',
                                $user->id,]) ?>"> 
                        <?= $this->Html->image($user->profile_pic,['class' => 'profile_pic']) ?>
                        <?= ucwords($user->first_name) ?>
                        <small class="text-muted">&#8226; <?= get_timediff($tweet->created) ?> </small>
                    </i>
                </a>
                <div class="card mt-3">
                <div class="card-body m-auto">
                    <?php if ($user_id == $tweet->id): ?>
                        <div class="dropdown" style="position:absolute; right:0; top:0;">
                            <i style="color:gray; cursor:pointer;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="" data-toggle="modal" data-target="#tweetModal<?= $tweet->id ?>">Edit Post</a>
                                <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'tweets',
                                    'action'=>'delete',
                                    $tweet->id,]) ?>">Delete Post
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($tweet->image): ?>
                            <div class="card-body tweet-content">
                                <p class="card-text text-left"><?= h($tweet->content) ?></p>
                            </div>
                            <?= $this->Html->image($tweet->image, ['class' => 'card-img-bottom']) ?>
                        <?php else: ?>
                            <p class="card-text"><?= h($tweet->content) ?></p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="tweets-btn">
                        <?php if ($tweet->likes_userid){
                            echo "<i class='fa fa-thumbs-up text-primary'>";
                            echo $this->Form->postLink('likes', [
                            'controller' => 'likes',
                            'action' => 'delete', 
                            'class' => 'btn btn-warning fa fa-envelope',
                            'id' => 'likebtn',
                            $tweet->id]);
                        } else{
                            echo " <i class='fa fa-thumbs-up text-secondary'> ";
                            echo $this->Form->postLink('likes', [
                                'controller' => 'likes',
                                'action' => 'add', 
                                'class' => 'btn btn-warning fa fa-envelope',
                                'id' => 'likebtn',
                                $tweet->id]);
                        }  
                        ?>
                        </i>
                        <a class="fa fa-comment" style="color:gray;" href="<?=$this->Url->build([
                            'controller' => 'comments',
                            'action' => 'view',
                            $tweet->id,
                        ]);?>"> Comment</a>
                        <a class="fa fa-retweet" style="color:gray;" data-toggle="modal" data-target="#retweetModal<?= $tweet->id ?>"> Retweet</a> 
                    </div>
        <?php foreach ($tweets as $tweet): ?>
            <div class="card-body w-50" style="margin:auto;">
                <a href="<?= $this->Url->build(['controller'=>'users',
                                'action'=>'profile',
                                $tweet->comment['user_id']]) ?>"> 
                        <?= $this->Html->image($tweet->user['profile_pic'],['class' => 'profile_pic']) ?>
                        <?= ucwords($tweet->user['first_name']) ?>
                        <small class="text-muted">&#8226; <?= get_timediff($tweet->comment['created']) ?> </small>
                    </i>
                </a>
                <div class="card mt-3">
                <div class="card-body" style="margin:auto;">
                    <?php if ($user_id == $tweet->comment['user_id']): ?>
                        <div class="dropdown" style="position:absolute; right:0; top:0;">
                            <i style="color:gray; cursor:pointer;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'comments',
                                    'action'=>'delete',
                                    $tweet->comment['id'],$tweet_id]) ?>">Delete Comment
                                </a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <p class="card-text"><?= $tweet->comment['content'] ?></p>
                    </div>
                </div>
                <!-- <div class="tweets-btn">
                    <i class="fa fa-thumbs-up" style="color:gray;"> Likes</i> 
                </div> -->
            </div>
        <?php endforeach; ?>
        <hr>
        <div class="w-25" style="margin:auto;"> 
            <?= $this->Form->create(null, [
                'url' => [
                        'controller' => 'comments',
                        'action' => 'add',
                        $tweet_id
                        ]]) ?>
                <?= $this->Form->control('content',['label' => false, 'placeholder' => 'Write a comment...', 'autocomplete' => 'off', 'maxlength'=>'140', 'required' => true]) ?>
                <?= $this->Form->submit(_('Post Comment')) ?>
            <?= $this->Form->end() ?></div>             
        </div>
    </div>
<?php include 'updateModal.php' ?>
<?php include 'retweetModal.php' ?>  
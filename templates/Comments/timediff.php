<?php   
    function get_timediff($time){
        $start_date = new \DateTime($time);
        $date_diff = $start_date->diff(new \DateTime());
        if ($date_diff->days){
            echo h($date_diff->days).'d<br>';
        } else if ($date_diff->m){
            echo h($date_diff->m).'mth<br>';
        } else if ($date_diff->y){
            echo h($date_diff->y).'yr<br>';
        } else if ($date_diff->i){
            echo h($date_diff->i).'m<br>';
        } else if ($date_diff->i>60){
            echo h($date_diff->h).'h<br>';
        } else if ($date_diff->h){
            echo h($date_diff->h).'h<br>';
        } else if ($date_diff->s){
            echo 'secs ago<br>';
        }
    }
?>
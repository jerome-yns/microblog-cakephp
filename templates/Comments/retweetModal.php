<div class="modal fade" id="retweetModal<?= $tweet->id ?>" tabindex="-1" role="dialog" aria-labelledby="tweetTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h4 class="modal-title" id="exampleModalLongTitle">Retweet</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null, [
                        'url' => [
                            'controller' => 'retweets',
                            'action' => 'add',
                            $tweet->id
                        ]]) ?>
                    <?= $this->Form->control('content',['label' => false, 'placeholder' => "What's on your mind?", 'autocomplete' => 'off', 'maxlength'=>'140', 'required' => true]) ?>
                        <div class="card">
                            <div class="card-body w-100">
                                <i class="fa fa-user-circle fa-lg w-100 text-left"><?= ucwords($user->first_name) ?></i>
                            <div class="card">
                                <div class="card-body">
                                    <p class="card-text"><?= $tweet->content ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->submit(_('Retweet')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div> 
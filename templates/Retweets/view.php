<?php include 'timediff.php' ?>
<div class="container container-sm">
    <div class="card">
        <div class="card-body" id="post-content"> 
            <?php foreach ($tweets as $tweet): ?>
                <div class="card" style="border:none;">
                    <div class="card-body w-75 retweet-body">
                        <a href="<?= $this->Url->build(['controller'=>'users',
                            'action'=>'profile',
                            $tweet->user_id]) ?>"> 
                            <?= $this->Html->image($tweet->retweet_user['profile_pic'],['class' => 'profile_pic']) ?>
                            <?= h(ucwords($tweet->retweet_user['first_name'])) ?>
                            <small class="text-muted">&#8226; <?= h(get_timediff($tweet->created)) ?> </small></i></a>
                        <?php if($user_id == $tweet->user_id): ?>
                            <div class="dropdown retweet-menu">
                                <i id="dropdownMenuButton" class="post-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                                <div class="dropdown-menu text-center" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="" data-toggle="modal" data-target="#tweetModal<?= h($tweet->retweets['id']) ?>">Edit Post</a>
                                    <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'retweets', 'action'=>'delete', $tweet->retweets['id']]) ?>">Delete Post</a>
                                </div>
                            </div>
                    </div>
                        <?php endif; ?>
                    <p class="text-left w-75 m-auto"> <i class="fa fa-retweet" class="rtcontent-logo"></i><?= h($tweet->content) ?></p><br/>
                    <div class="w-75 m-auto retweet-content">
                        <a href="<?= $this->Url->build(['controller'=>'users',
                            'action'=>'profile',
                            $tweet->Tweets['user_id']]) ?>"> 
                            <?= $this->Html->image($tweet->user['profile_pic'],['class' => 'profile_pic']) ?>
                            <?= h(ucwords($tweet->user['first_name'])) ?>
                            <small class="text-muted">&#8226; <?= h(get_timediff($tweet->created)) ?> </small></i></a>
                        <a href="<?=$this->Url->build([
                            'controller' => 'comments',
                            'action' => 'view',
                            $tweet->tweet_id
                        ]);?>">
                        <div class="card mt-3">
                            <div class="card-body m-auto">
                                <?php if ($tweet->Tweets['image']): ?>
                                    <div class="card-body tweet-content">
                                        <p class="card-text text-left"><?= h($tweet->Tweets['content']) ?></p>
                                    </div>
                                    <?= $this->Html->image($tweet->Tweets['image'],['class' => 'card-img-bottom']) ?>
                                <?php else: ?>
                                    <p class="card-text"><?= h($tweet->Tweets['content']) ?></p>
                                <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div> </a> <br/>
                    <div class="tweets-btn w-75 m-auto">
                        <?php if ($tweet->likes_userid){
                            echo "<i class='fa fa-thumbs-up text-primary'>";
                            echo $this->Form->postLink('likes', [
                            'controller' => 'likes',
                            'action' => 'delete', 
                            'class' => 'btn btn-warning fa fa-envelope',
                            'id' => 'likebtn',
                            $tweet->likes['id'],$tweet->retweets['id']]);
                        } else {
                            echo " <i class='fa fa-thumbs-up text-secondary'> ";
                            echo $this->Form->postLink('likes', [
                                'controller' => 'likes',
                                'action' => 'add', 
                                'class' => 'btn btn-warning fa fa-envelope',
                                'id' => 'likebtn',
                                $tweet->tweet_id, $tweet->id]);
                        } ?></i>
                        <a class="fa fa-comment" href="<?= $this->Url->build([
                            'controller' => 'retweets',
                            'action' => 'view',
                            $tweet->id,
                        ]);?>"> Comment</a>
                        <a class="fa fa-retweet" data-toggle="modal" data-target="#retweetModal<?= h($tweet->id) ?>"> Retweet</a> 
                    </div>
                </div>
            </div>
         <?php endforeach; ?>
         <div class="card" style="background-color:white;">
            <div class="card-body w-50 m-auto">
            <?php foreach ($comments as $comment): ?>
                    <a href="<?= $this->Url->build(['controller'=>'users',
                        'action'=>'profile',
                        $comment->user_id,]) ?>"> 
                        <?= $this->Html->image($comment->user['profile_pic'],['class' => 'profile_pic']) ?>
                        <?= h(ucwords($comment->user['first_name'])) ?>
                        <small class="text-muted">&#8226; <?= h(get_timediff($comment->comment['created'])) ?> </small></i></a>
                <div class="card mt-3">
                    <div class="card-body m-auto">
                        <?php if ($user_id == $comment->comment['user_id']): ?>
                            <div class="dropdown retweet-menu" >
                                <i id="dropdownMenuButton" class="post-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">&#8226;&#8226;&#8226;</i>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="<?= $this->Url->build(['controller'=>'comments',
                                        'action'=>'delete',
                                        $comment->comment['id'], $comment->id, $comment->comment['retweet_id'] ]) ?>">Delete Comment</a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <p class="card-text"><?= h($comment->comment['content']) ?></p>
                        </div>
                    </div>
            <?php endforeach; ?>
            </div>
                <div class="w-50" style="margin:auto;"> 
                    <?= $this->Form->create(null, [
                        'url' => [
                            'controller' => 'comments',
                            'action' => 'add',
                            $tweet->tweet_id, $tweet->id
                            ]]) ?>
                        <?= $this->Form->control('content', ['label' => false, 'placeholder' => 'Write a comment...', 'autocomplete' => 'off', 'maxlength'=>'140', 'required' => true]) ?>
                        <?= $this->Form->submit(_('Post Comment')) ?>
                    <?= $this->Form->end() ?>
                </div>             
            </div>
        </div>
    </div>
</div>
<?php include 'updateModal.php' ?>
<?php include 'retweetModal.php' ?>
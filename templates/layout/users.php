<!DOCTYPE html>
<html lang="en">
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Microblog</title>
        <?= $this->Html->meta('icon') ?>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

        <?= $this->Html->css(['normalize.min', 'milligram.min', 'cake']) ?>
        <?= $this->Html->script ?>
    </head>
    <body>
        <?php if($this->request->getParam('controller')== 'Features')
            echo $this->element('nav')  
        ?>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content')  ?>
    </body>
</html>


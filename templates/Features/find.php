<?= $this->Html->css('style') ?>
<div class="container full-height d-flex align-items-center justify-content-center">
    <div class="card w-50">
        <div class="card-header">
            <h3>Find your account</h3>
        </div>
        <div class="card-body ">
            <?php echo $this->Form->create(); ?>
                <?php echo $this->Form->control('email', ['placeholder' => 'Enter Email', 'label' => false, 'required' => true, 'autocomplete' => 'off']); ?>
                <div class="d-flex justify-content-end">
                <a href="<?= $this->Url->build(['controller'=>'users',
                    'action'=>'register',
                    ]) ?>">
                    <?= $this->Form->button('Cancel',['type' => 'button', 'class' => 'btn btn-secondary']) ?>
                </a> &nbsp;
                <?= $this->Form->button('Send reset link',['name' => 'reset', 'class' => 'btn btn-success']) ?>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
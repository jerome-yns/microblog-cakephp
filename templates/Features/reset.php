<?= $this->Html->css('style') ?>
<div class="container full-height d-flex align-items-center justify-content-center">
    <div class="card w-50">
        <div class="card-header">
            <h3>Reset</h3>
        </div>
        <div class="card-body ">
            <?php echo $this->Form->create(); ?>
                <?php echo $this->Form->control('password', ['placeholder' => 'New Password', 'label' => false,'required' => true]); ?>
                <div class="d-flex justify-content-end">
                <a href="<?= $this->Url->build(['controller'=>'users',
                    'action'=>'login',
                    ]) ?>">
                    <?= $this->Form->button('Cancel', ['type' => 'button','class' => 'btn btn-secondary']) ?>
                </a> &nbsp;
                <?= $this->Form->button('Reset', ['name' => 'reset']) ?>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark " style="display:flex; align-items:center; justify-content:space-between;">
    <div class="navbar-nav">
      <li class="nav-item " style="display:flex; align-items:center; ">
        <a class="nav-link " href="<?= $this->Url->build(['controller' => 'Users' ,'action' => 'index']) ?>">Microblog</a>
      </li>
      <?php if ($name): ?>
        <div class="search-bar">
          <?= $this->Form->create(null, [
                  'url' => [
                      'controller' => 'Users',
                      'action' => 'search',
                  ],'type' => 'get']) ?>
                <div class="d-flex align-items-center justify-content-center">
                <?= $this->Form->control('key',['id' => 'search','autocomplete' => 'off', 'label'=> false, 'placeholder' => 'Search']); ?>
                <select class="p-0 m-0" id="searchfilter">
                      <option value="users" selected>Search:Users</option>
                      <option value="tweets">Search:Tweets</option>
                      <option value="retweets">Search:Retweets</option>
                </select>
            <?= $this->Form->end(); ?>
        </div>
      <?php endif; ?>
    </div>
    </div class="nav-right col-md-1">
        <div class="navbar-profile">
          <div class="mr-3" id="nav-user">
            <a  href="<?= $this->Url->build(['controller'=>'users','action' => 'profile' ,$user_id])?>">
            <?php if ($user_image): ?>
              <?= $this->Html->image($user_image,['class' => 'nav_userimage']) ?>
            <?php else: ?>
              <i class="fa fa-user-circle fa-lg " id="nav-profile"> 
            <?php endif; ?>
            <span><?= h(ucwords($name)) ?></span></i> </a>
          </div>
          <div class="dropdown">
            <i class=" fa fa-caret-down fa-lg" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
              <div class="dropdown-menu"style="right: 0; left: auto;" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item p-2 text-center" href="<?= $this->Url->build(['controller'=>'users','action' => 'logout']) ?>">Logout</a>
            </div>
          </div>
        </div>
        <div class="dropdown menu-bar">
            <i class="fa fa-bars fa-lg" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
              <div class="dropdown-menu"style="right: 0; left: auto;" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item p-2 text-center" href="<?= $this->Url->build(['controller'=>'users','action' => 'profile',$user_id]) ?>">Profile</a>
                <a class="dropdown-item p-2 text-center" href="<?= $this->Url->build(['controller'=>'users','action' => 'list']) ?>">Search</a>
                <a class="dropdown-item p-2 text-center" href="<?= $this->Url->build(['controller'=>'users','action' => 'logout']) ?>">Logout</a>
            </div>
          </div>
    </div>
</nav>
<div class="col-md-2" style="position:absolute; margin-left:60px; z-index:1;">
  <div class="list-group" id="show-list">
        
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#search').keypress(function(e){
    if ( e.which == 13 ) return false;
  }); 
    $('select').click(function(){
        $('#search').val('');
    });
    $('#search').keyup(function(){
      if(!searchText){
        var e = document.getElementById("searchfilter");
        var options = e.value;
        var searchText = $(this).val();
        var url;
        if (options === 'users') {
          url = "<?= $this->Url->build(['controller' => 'users', 'action' => 'search']) ?>";
        } else if  (options === 'tweets') {
          url = "<?= $this->Url->build(['controller' => 'tweets', 'action' => 'search']) ?>";
        } else if  (options === 'retweets'){
          url = "<?= $this->Url->build(['controller' => 'retweets', 'action' => 'search']) ?>";
        }
        $.ajax({
          url: url,
          method:'post',
          data:{query:searchText},
          headers:{
              'X-CSRF-Token': $('meta[name="csrfToken"]').attr('content')
          },
          success:function(response){
            $('#show-list').html(response);
          }
        })
      }
      else{
        $('#show-list').html('');
      }
      
    });
  })
</script>
<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\Event\EventInterface;

/**
 * Validation Controller
 *
 * @method \App\Model\Entity\Validation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ValidationController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */

     
    public function beforeFilter(EventInterface $event){
        $this->viewBuilder()->setLayout('users');
    }
    public function home(){
    }

    public function register(){
        $this->loadModel('Users');
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $password = $this->request->getData('password');
            $confirmPassword = $this->request->getData('confirm_password');
            if($password === $confirmPassword){
                $user = $this->Users->patchEntity($user, $this->request->getData());
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('The user has been saved.'));
                    return $this->redirect(['action' => 'home']);
                } else{
                    $this->Flash->error(__('The user could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->error(__('Password did not match'));
            }
           
        }
        $this->set(compact('user'));



        // $mailer = new Mailer();
        // $mailer->setFrom(['test@test.com' => 'My Site'])
        //     ->setTo('johnjeromebernal.yns@gmail.com')
        //     ->setSubject('About')
        //     ->deliver();

   
        // TransportFactory::setConfig('mailtrap', [
        //     'host' => 'smtp.mailtrap.io',
        //     'port' => 2525,
        //     'username' => '07ddbc752eab43',
        //     'password' => '0c6003fe8867ca',
        //     'className' => 'Smtp'
        //   ]);

    }
    public function index()
    {
        $validation = $this->paginate($this->Validation);

        $this->set(compact('validation'));
    }

    /**
     * View method
     *
     * @param string|null $id Validation id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $validation = $this->Validation->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('validation'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $validation = $this->Validation->newEmptyEntity();
        if ($this->request->is('post')) {
            $validation = $this->Validation->patchEntity($validation, $this->request->getData());
            if ($this->Validation->save($validation)) {
                $this->Flash->success(__('The validation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The validation could not be saved. Please, try again.'));
        }
        $this->set(compact('validation'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Validation id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $validation = $this->Validation->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $validation = $this->Validation->patchEntity($validation, $this->request->getData());
            if ($this->Validation->save($validation)) {
                $this->Flash->success(__('The validation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The validation could not be saved. Please, try again.'));
        }
        $this->set(compact('validation'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Validation id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $validation = $this->Validation->get($id);
        if ($this->Validation->delete($validation)) {
            $this->Flash->success(__('The validation has been deleted.'));
        } else {
            $this->Flash->error(__('The validation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
